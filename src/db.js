'use strict';

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const uuidv4 = require('uuid/v4');

const { stationCollection } = require('./station/station.js');
const { userCollection } = require('./user/user.js');

const DB_COLLECTIONS = [stationCollection, userCollection];
const databaseDefault = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@muhiscluster0-i3zaw.mongodb.net/admin?retryWrites=true&w=majority`;
const databaseUrl = process.env.DB_URL || databaseDefault;
const OPTIONS = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};

const weeklytop = [null, null, null, null, null,];

/**
 * Initializes the database. Performs clean initialization if DB_CLEAN
 * environment flag is set to true.
 */
const initDb = function initDb() {

    process.env.DB_CLEAN = process.env.DB_CLEAN || true;

    MongoClient.connect(databaseUrl, OPTIONS)
        .then(async (client, err) => {

            assert(!err);
            assert(client);

            if(!process.env.DB_NAME) {
                throw new ReferenceError("Database name not defined!");
            }

            let db = client.db(process.env.DB_NAME);
            console.log('-- Database connection established --\n');

            if (process.env.DB_CLEAN) {
                await cleanDb(db)
                    .then(() => client.close);
            } else {
                await client.close(true);
            }
            let time = new Date().toUTCString();
            console.log(`-- Database init completed -- ${time}\n`);
        })
        .catch(e => {
            if(e instanceof assert.AssertionError) {
                // Connection error -> retry
                console.log(e);
                setTimeout(initDb, 1500);
            } else if (e instanceof ReferenceError) {
                // Program settings wrong -> exit
                console.log(e);
                process.exit(1);
            } else {
                throw e;
            }
        });
};

/**
 * Get all stations from 'stations' collection
 * @return {Promise<Object[]>} - Array of station objects
 */
/*const getAllStations = function() {

    return MongoClient.connect(databaseUrl, OPTIONS)
        .then((client, err) => {
            assert(!err);

            assert(process.env.DB_NAME);

            let db = client.db(process.env.DB_NAME);
            console.log('\n-- Database connection established --\n');

            if (process.env.DB_CLEAN) {
                await cleanDb(db)
                    .then(() => {
                        process.env.DB_CLEAN = false;
                        client.close(true);
                        let time = new Date().toUTCString();
                        console.log(`\n-- Database init completed -- ${time}\n`);
                    })
                    .catch(e => {
                        throw e;
                    })
            } else {
                client.close(true);
            }
        })
        .catch(e => {
            throw e;
        });
};*/

/**
 * Get all stations from 'stations' collection
 * @return {Promise<Object[]>} - Array of station objects
 */
const getAllStations = function() {

    return MongoClient.connect(DB_URI, OPTIONS)
        .then((client, err) => {

            assert(!err);

            let db = client.db(process.env.DB_NAME);

            return db.collection('stations')
                .find({})
                .toArray()
                .then((result, err) => {

                    assert(!err);

                    return client.close(true)
                        .then(() => {
                            return result.map(station => {
                                return {
                                    name: station.name,
                                    latitude: station.latitude,
                                    longitude: station.longitude,
                                    currentMonkeys: station.currentMonkeys,
                                    DEBUGXIITTIKOODI: station.stationId
                                }
                        })
                    })
                })
                .catch(e => {
                    throw e
                })
        });
};

/**
 * Add a monkey to station.
 * TODO: Aggregation.
 * @param monkey - Monkey info - userID and stationID
 * @return {Promise<T>}
 */
const addMonkey = function (monkey) {

    return MongoClient.connect(databaseUrl, OPTIONS)
        .then((client, err) => {

            assert(!err);

            let db = client.db(process.env.DB_NAME);

            return db.collection('stations')
                .find({ stationId: monkey.stationId })
                .limit(1).next()
                .then((station, err) => {
                    assert(!err);
                    assert(station);
                    if(station.currentMonkeys <= 5 &&
                        station.currentUsers.filter(user => user.userId === monkey.userId).length === 0) {
                        return db.collection('stations')
                            .findOneAndUpdate(
                                { _id: station._id },
                                {
                                    $push: { currentUsers: {userId: monkey.userId, timeStamp: new Date()}},
                                    $inc: {"currentMonkeys": 1}
                                },
                                { returnOriginal:false })
                    } else {
                        throw {code: "Cant add user to station!"};
                    }
                })
                .then((result, err) =>  {

                    assert(!err);
                    assert(result);

                    return client.close(true)
                        .then(() => {
                            return {
                                currentMonkeys: result.value.currentMonkeys,
                            };
                        })
                })
        })
        .catch(e => {
            throw e;
        })
};

/**
 * Find and modify user age. Find by userID.
 * @param age - User new age.
 * @param userId - UserID
 * @return {Promise<Object>} - Updated user object (advanced)
 */
function modifyUser(age, userId) {

    return MongoClient.connect(databaseUrl, OPTIONS)
        .then((client, err) => {

            assert(!err);

            let db = client.db(process.env.DB_NAME);

            return db.collection('users')
                .findOneAndUpdate(
                    {userId: userId},
                    {$set: {age: age}},
                    { "returnOriginal": false })
                .then((result, err) =>  {

                    assert(!err);
                    assert(result);

                    return client.close(true)
                        .then(() => {
                            return {
                                userId: result.value.userId,
                                email: result.value.email,
                                name: result.value.name,
                                age: result.value.age,
                            };
                        })
                })
        })
        .catch(e => {
            throw e;
        });
};

/**
 * Delete user from 'users' collection by userID.
 * @param user - User object.
 * TODO: Require password. Refactor
 * @return {Promise<Object>} - Operation completed.
 */
function deleteUser(user) {

    return MongoClient.connect(databaseUrl, OPTIONS)
        .then((client, err) => {

            assert(!err);

            let db = client.db('restapina');

            return db.collection('users')
                .deleteOne( {userId: user.userId })
                .then((result, err) => {

                    assert(!err);

                    return client.close(true)
                        .then(() => {
                            return {user: 0}
                        })
                })
        })
}


/**
 * Search user by userID from 'users' collection.
 * @param uuid - UserID
 * @return {Promise<Object>} - User object (basic)
 */
function getUserById(uuid) {
    return MongoClient.connect(DB_URI, OPTIONS)
        .then((client, err) => {
            assert(!err);

            let db = client.db(process.env.DB_NAME);

            return db.collection('users')
                .find({userId : uuid})
                .then((result, err) => {

                    assert(!err);

                    return client.close(true)
                        .then(() => {
                            return {
                                name: result.name,
                                age: result.age,
                            }
                        })
                });
        })
        .catch(e => {
            throw e
        })
}

/**
 * Search user by name from 'users' collection.
 * @param name - User name
 * @return {Promise<Object>} - User object (basic)
 */
function getUserByName(name) {
    return MongoClient.connect(databaseUrl, OPTIONS)
        .then((client, err) => {

            assert(!err);

            let db = client.db('restapina');

            return db.collection('users')
                .find({name: name})
                .limit(1).next()
                .then((result, err) => {

                    assert(!err);

                    return client.close(true)
                        .then(() => {
                            return result.map(user => ({name: user.name, age: user.age}));
                        })
                });
        })
        .catch((e) => {
            throw e;
        })
}

/**
 * Adds a new user to 'users' collection.
 * Returns advanced user object (includes userID).
 * @param user - User info
 * @return {Promise<Object>} - User object (advanced).
 */
function addUser(user) {
    return MongoClient.connect(databaseUrl, OPTIONS)
        .then((client, err) => {
            assert(!err);

            let db = client.db(process.env.DB_NAME);

            return db.collection('users')
                .insertOne({
                    userId: uuidv4(),
                    email: user.email,
                    name: user.name,
                    age: user.age,
                    pass: user.pass
                })
                .then((result, err) => {
                    assert(!err);
                    assert(result);
                    return client.close(true)
                        .then(() => {
                            return {
                                userId: result.ops[0].userId,
                                email: result.ops[0].email,
                                name: result.ops[0].name,
                                age: result.ops[0].age
                            };
                        });
                })
        })
        .catch(e => {
            throw e;
        });
}

/**
 * Search user from database with email and pass.
 * Returns advanced user object (includes userID).
 * @param email - User email
 * @param pass - User pass
 * @return {Promise<Object>} - User object (advanced).
 */
const login = function(email, pass) {
    return MongoClient.connect(databaseUrl, OPTIONS)
        .then((client, err) => {
            assert(!err);

            let db = client.db(process.env.DB_NAME);

            return db.collection('users')
                .findOne({email: email, pass: pass})
                .then((result, err) =>  {
                    assert(!err);
                    assert(result);
                    return client.close(true)
                        .then(() => {
                            return {
                                userId: result.userId,
                                email: result.email,
                                name: result.name,
                                age: result.age,
                            };
                        })
                })
        })
        .catch(e => {
            throw e;
        })
};

/**
 * Iterates through 'stations' collection finding any stations that have
 * 'userId' in the currentUsers array.
 * @param userId
 * @return {Promise<[Object]>} - Station objects
 */
const getStationsWithUser = function(userId) {

    return MongoClient.connect(databaseUrl, OPTIONS)

        .then((client, err) => {

            assert(!err);

            let db = client.db('restapina');

            return db.collection('stations')
                .find({ currentUsers: { $elemMatch: { userId: userId } } })
                .toArray()
                .then((result, err) => {

                    assert(!err);

                    return client.close(true)
                        .then(() => {
                            let stations = {};
                            stations.captured = result.map(station => station.name);
                            return stations;
                        })
                });
        })
        .catch((e) => {
            throw e;
        })
};


// MARK FOR REMOVE
/**
 * Search the 'station' collection by station name
 * @param name
 * @return { Promise<Object> }
 */
const getStationByName = function(name) {
    return MongoClient.connect(databaseUrl, OPTIONS)
        .then((client, err) => {

            assert(!err);

            let db = client.db('restapina');

            return db.collection('stations')
                .findOne({ name: name })
                .then((result, err) => {
                    return client.close(true)
                        .then(() => {
                            return result;
                        })
                });
        })
        .catch((e) => {
            throw e;
        })
};

/**
 * Finds all users from station by station name.
 * TODO: Aggregation
 * @param name
 * @return {Promise<[Object]>} - User objects.
 */
const getUsersInStation = function(name) {
    return MongoClient.connect(databaseUrl, OPTIONS)
        .then((client, err) => {

            assert(!err);

            let db = client.db('restapina');

            return db.collection('stations')
                .findOne({ name: name })
                .then((result, err) => {

                    assert(!err);

                    return db.collection('users')
                        .find({
                            userId: {
                                $in: result.currentUsers.map(user => user.userId)
                            }})
                        .toArray();
                })
                .then((result, err) => {

                    assert(!err);

                    return client.close(true)
                        .then(() => {
                            return {
                                users:
                                    result.map((user) => {
                                        return {
                                            name: user.name,
                                            age: user.age,
                                        }})
                            }
                        })
                })
        })
        .catch(e => {
            throw e;
        })
};

function getTopList() {
    return MongoClient.connect(databaseUrl, OPTIONS)
        .then((client, err) => {
            assert(!err);

    })
}

/**
 * Drops all the collections from the database.
 * @param db
 * @return {Promise<Boolean[]>}
 */
function dropDb(db) {
    return db.collections()
        .then((result, err) => {
            assert(!err);
            return Promise.all(result.map(c => db.dropCollection(c.collectionName)));
        })
        .catch(e => {
            throw e;
        })
}

/**
 * Inserts every collection from DB_COLLECTIONS to database.
 * @param db
 * @return {Promise<Boolean[]>}
 */
function insertCollections(db) {
    let tasks = [];
    DB_COLLECTIONS.forEach(newCollection => {
        tasks.push(
            db.createCollection(newCollection.name, newCollection.options)
                .then((collection, err) => {
                    assert(!err);
                    return newCollection.init(collection);
                })
                .catch(e => {
                    throw e
                })
        )
    });
    return Promise.all(tasks);
}

/**
 * Drops collections and initializes new collections from scratch.
 * @async
 * @param db
 * @return {Promise<Boolean[]>}
 */
async function cleanDb(db) {
    return await dropDb(db)
        .then(values => {
            assert(values.every(x => x === true));
            let time = new Date().toUTCString();
            console.log(`-- Database dropping completed -- ${time}\n`);
        })
        .then(() => insertCollections(db))
        .then(values => {
            let time = new Date().toUTCString();
            console.log(`-- Collections init completed -- ${time}\n`);
        })
    /*const DB_DROP =
        dropDb(db)
            .then(values => {
                assert(values.every(x => x === true));
                let time = new Date().toUTCString();
                console.log(`\n-- Database dropping completed -- ${time}\n`);
            })
            .catch(e => {
                throw e;
            });

    const DB_INSERTCOLLECTIONS =
        insertCollections(db)
            .then(values => {
                let time = new Date().toUTCString();
                console.log(`\n-- Collections init completed -- ${time}\n`);
            })
            .catch(e => {
                throw e;
            });

    return await Promise.all([DB_DROP, DB_INSERTCOLLECTIONS]);*/
}


/**
 * Database maintenance control module.
 * @module DbControl
 * @type {{
 *  initDb: function
 *  }}
 */
module.exports.dbInit = initDb;

/**
 * Database user operation module.
 * @module DbUser
 * @type {{
 *      add: function,
 *      modify: function,
 *      getById: function,
 *      getByName: function,
 *      delete: function
 *      }}
 */
module.exports.userDb = {
    delete: deleteUser,
    getById: getUserById,
    getByName: getUserByName,
    modify: modifyUser,
    add: addUser,
    getWeekBest: getTopList,
};

/**
 * DataBase login operation module.
 * @module DbLogin
 * @type {{
 *  login: function
 *  }}
 */
module.exports.loginDb = {
    login: login,
};

/**
=======

/**
 * Inserts every collection from DB_COLLECTIONS to database.
 * @param db
 * @return {Promise<Boolean[]>}
 */
function insertCollections(db) {
    let tasks = [];
    DB_COLLECTIONS.forEach(newCollection => {
        tasks.push(
            db.createCollection(newCollection.name, newCollection.options)
                .then((collection, err) => {
                    assert(!err);
                    return newCollection.init(collection);
                })
                .catch(e => {
                    throw e
                })
        )
    });
    return Promise.all(tasks);
}

/**
 * Drops collections and initializes new collections from scratch.
 * @async
 * @param db
 * @return {Promise<unknown[]>}
 */
async function cleanDb(db) {
    const DB_DROP =
        dropDb(db)
            .then(values => {
                assert(values.every(x => x === true));
                let time = new Date().toUTCString();
                console.log(`\n-- Database dropping completed -- ${time}\n`);
            })
            .catch(e => {
                throw e;
            });

    const DB_INSERTCOLLECTIONS =
        insertCollections(db)
            .then(values => {
                let time = new Date().toUTCString();
                console.log(`\n-- Collections init completed -- ${time}\n`);
            })
            .catch(e => {
                throw e;
            });

    return await Promise.all([DB_DROP, DB_INSERTCOLLECTIONS]);
}


/**
 * Database maintenance control module.
 * @module DbControl
 * @type {{
 *  initDb: function
 *  }}
 */
module.exports.dbInit = () => {
    initDb();
};

/**
 * Database user operation module.
 * @module DbUser
 * @type {{
 *      add: function,
 *      modify: function,
 *      getById: function,
 *      getByName: function,
 *      delete: function
 *      }}
 */
module.exports.userDb = {
    delete: deleteUser,
    getById: getUserById,
    getByName: getUserByName,
    modify: modifyUser,
    add: addUser,
};

/**
 * DataBase login operation module.
 * @module DbLogin
 * @type {{
 *  login: function
 *  }}
 */
module.exports.loginDb = {
    login: login,
};

/**
>>>>>>> dcec42046f00cfaf1d6ef099190d973a6367c0ee
 * Database station operation module.
 * @module DbStation
 * @type {{
 *  addMonkey: function,
 *  getAll: function,
 *  getUsers: function,
 *  getWithUser: function
 *  }}
 */
module.exports.stationDb = {
    addMonkey: addMonkey,
    getAll: getAllStations,
    getUsers: getUsersInStation,
    getWithUser: getStationsWithUser,
};
