'use strict';

const http = require('http');
const http2 = require('http2');
const fs = require('fs');
const db = require('./db');

const requestListener = require('./requestListener.js');

const PRIMARY_PORT = process.env.PORT_PRIMARY || 443;
const SECONDARY_PORT = process.env.PORT_SECONDARY || 80;
const options = {
    key: fs.readFileSync('./cert/ecsda/private.pem'),
    cert: fs.readFileSync('./cert/ecsda/restapina-cert.pem'),
};

try {
    db.dbInit();
} catch (e) {
    console.log('e1', e);
    // Something wrong with connection --> Do the needful
}

http2
    .createSecureServer(options, requestListener)
    .listen(PRIMARY_PORT, () => {
        console.log(`\n{ Listening } HTTP2 on port ${PRIMARY_PORT}`);
    });

http
    .createServer(function (req, res) {
        res.writeHead(301,
            { "Location": `https://${req.headers.host}:443` });
        res.end();
    }).listen(SECONDARY_PORT, () => {
    console.log(`{ Listening } { Redirecting } HTTP on port ${SECONDARY_PORT}\n`);
});