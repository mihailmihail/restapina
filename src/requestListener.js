const Url = require('url');
const fs = require('fs');
const path = require('path');
const { loginRoute } = require('./login/login.js');
const { userRoute } = require('./user/route.js');
const { stationRoute } = require('./station/route.js');

const FAVICON = path.join(__dirname, '../public', 'favicon.ico');
const INDEX = path.join(__dirname, '../public', 'index.html');
const STYLE = path.join(__dirname, '../public', 'style.css');
const NORMALIZE = path.join(__dirname, '../public', 'normalize.css');
const BG = path.join(__dirname, '../public', 'bg.jpg');
const ICON_STAR = path.join(__dirname, '../public', 'basic_star.svg');
const TESTSITE = path.join(__dirname, '../public', 'testsite.html');

const parseParam = function(query) {
    let parameters = {};

    decodeURI(query)
        .split('&')
        .forEach(query => {
            let [field, value] = query.split('=');
            parameters[field] = value
        });

    return parameters
};

const requestListener = function requestListener(req, res) {

    let url = Url.parse(req.url);
    let pathname = url.pathname.split('/')[1];
    let requestBody = '';

    try {
        switch (pathname) {
            case 'index':
                res.writeHead(200, {'Content-Type': 'text/html'});
                fs.createReadStream(INDEX).pipe(res);
                break;

            case 'admin':
                res.writeHead(200, {'Content-Type': 'text/html'});
                fs.createReadStream(TESTSITE).pipe(res);
                break;

            case 'favicon.ico':
                res.writeHead(200, {'Content-Type': 'image/x-icon'});
                fs.createReadStream(FAVICON).pipe(res);
                break;

            case 'bg.jpg':
                res.writeHead(200, {'Content-Type': 'image/x-icon'});
                fs.createReadStream(BG).pipe(res);
                break;

            case 'style.css':
                res.writeHead(200, {'Content-Type': 'text/css'});
                fs.createReadStream(STYLE).pipe(res);
                break;

            case 'normalize.css':
                res.writeHead(200, {'Content-Type': 'text/css'});
                fs.createReadStream(NORMALIZE).pipe(res);
                break;

            case 'basic_star.svg':
                res.writeHead(200, {'Content-Type': 'image/svg+xml'});
                fs.createReadStream(ICON_STAR).pipe(res);
                break;

            case 'user':

                res.setHeader('Content-Type', 'application/json');

                switch (req.method) {
                    case 'GET':

                        let parameters = parseParam(url.query);

                        userRoute.get(parameters)
                            .then(result => {
                                res.statusCode = 200;
                                res.end(JSON.stringify({ user: result }));
                            })
                            .catch(e => {
                                res.statusCode = 401;
                                res.end(JSON.stringify({ message: e.code }));
                            });
                        break;

                    case 'POST':
                        req.on('data', function (chunk) {
                            requestBody += chunk.toString();
                        });

                        req.on('end', function () {
                            userRoute.post(requestBody)
                                .then(result => {
                                    res.statusCode = 200;
                                    res.end(JSON.stringify({user: result}));
                                })
                                .catch(e => {
                                    console.log(e)
                                    res.statusCode = 401;
                                    res.end(JSON.stringify({message: e.code}));
                                });
                        });
                        break;

                    case 'PUT':
                        req.on('data', function (chunk) {
                            requestBody += chunk.toString();
                        });

                        req.on('end', function () {
                            userRoute.put(requestBody)
                                .then(result => {
                                    res.statusCode = 200;
                                    res.end(JSON.stringify({user: result}))
                                })
                                .catch(e => {
                                    res.statusCode = 400;
                                    res.end(JSON.stringify({message: e.code}));
                                })
                        });
                        break;

                    case 'DELETE':
                        req.on('data', function (chunk) {
                            requestBody += chunk.toString();
                        });

                        req.on('end', function () {
                            userRoute.remove(requestBody)
                                .then(result => {
                                    res.statusCode = 200;
                                    res.end(JSON.stringify({user: result}))
                                })
                                .catch(e => {
                                    res.statusCode = 400;
                                    res.end(JSON.stringify({message: e.code}));
                                })
                        });
                        break;

                    default:
                        res.statusCode = 204;
                        res.end();
                }
                break;

            case 'station':

                res.setHeader('Content-Type', 'application/json');

                switch (req.method) {

                    case 'GET':
                        stationRoute.get(url.query)
                            .then((result) => {
                                res.statusCode = 200;
                                res.end(JSON.stringify({ station: result }))
                            })
                            .catch((e) => {
                                console.log('e2', e);
                                res.statusCode = 400;
                                res.end(JSON.stringify({ message: e.code }))
                            });
                        break;
                    case 'POST':
                        req.on('data', (chunk) => {
                            requestBody += chunk.toString();
                        });
                        req.on('end', () => {
                            stationRoute.post(requestBody)
                                .then(result => {
                                    res.statusCode = 200;
                                    res.end(JSON.stringify({ station: result }))
                                })
                                .catch(e => {
                                    console.log(e)
                                    res.statusCode = 400;
                                    res.end(JSON.stringify({message: e.code}));
                                });
                        });
                        break;

                    case 'PUT':
                        res.statusCode = 200;
                        res.end();
                        break;

                    case 'DELETE':
                        res.statusCode = 200;
                        res.end();
                        break;
                    default:
                        res.statusCode = 500;
                }
                break;
            case 'login':
                if (req.method === 'GET' && req.headers.authorization) {
                    loginRoute.get(req.headers.authorization)
                        .then(result => {
                            res.statusCode = 200;
                            res.end(JSON.stringify({user: result}));
                        })
                        .catch(e => {
                            res.statusCode = 401;
                            res.end(JSON.stringify({message: e.code}));
                        });
                } else {
                    res.statusCode = 204;
                    res.end();
                }
                break;
            default:
                res.writeHead(200, {'Content-Type': 'text/html'});
                fs.createReadStream(INDEX).pipe(res);
        }
    } catch (e) {
        console.log(e)
        res.writeHead(400);
        res.end();
    }
};

module.exports = requestListener;