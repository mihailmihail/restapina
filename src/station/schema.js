const schema = {
    $jsonSchema: {
        bsonType: "object",
        required: ["stationId", "name", "latitude", "longitude", "currentMonkeys"],
        properties: {
            stationId: {
                bsonType: "string",
                description: "Must be string and is required"
            },
            name: {
                bsonType: "string",
                description: "Must be string and is required"
            },
            latitude:  {
                bsonType: "double",
                description: "Must be double and is required"
            },
            longitude: {
                bsonType: "double",
                description: "Must be double and is required"
            },
            currentMonkey: {
                bsonType: "string",
                description: "Player userId"
            }
        },
    }
};

module.exports = schema;