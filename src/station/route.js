const assert = require('assert');
const { stationDb } = require('../db.js');
console.log('sta', stationDb)

const route = {
    get: get,
    post: post,
    put: put,
    remove: remove
};

/**
 * Station get route.
 * @param query - URL query parameters.
 */
function get(query) {

    if (query) {
        query = decodeURI(query);
        let parameters = {};
        query
            .split('&')
            .forEach(parameter => {
                let [field, value] = parameter.split('=');
                parameters[field] = value;
            });

        if (parameters.currentuser) {
            return stationDb.getWithUser(parameters.currentuser);
        } else if (parameters.name) {
            return stationDb.getUsers(parameters.name);
        }
    } else {
        return stationDb.getAll();
    }
}

/**
 * Station post route.
 * @param requestBody
 * @return {*}
 */
function post(requestBody) {

    try {
        let monkey = JSON.parse(requestBody);

        assert(monkey.stationId);
        assert(monkey.userId);

        return stationDb.addMonkey(monkey);
    } catch (e) {
        throw e;
    }
}

/**
 * Station put route.
 * TODO: Currently no implementation.
 */
function put() {
    throw new Error('No such route')
}

/**
 * Station delete route.
 * TODO: Currently no implementation.
 */
function remove() {
    throw new Error('No such route')
}

module.exports.stationRoute = route;