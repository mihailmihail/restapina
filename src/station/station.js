'use strict';
const http = require('http');
const uuidv4 = require('uuid/v4');
const stationSchema = require('./schema.js');

const name =  'stations';

const databaseCfg = {
    validator: stationSchema,
    validationLevel: 'moderate',
    validationAction: 'warn'
}

/**
 * Gets station information from external API and inserts them to collection.
 * @param collection - mongodb Collection
 * @return {Promise}
 */
function init (collection) {
    const options = {
        hostname: 'api.citybik.es',
        port: 80,
        path: '/v2/networks/citybikes-helsinki',
        method: 'GET'
    };

    let result = '';

    return new Promise((resolve, reject) => {
        http.request(options, res => {

            res.on('data', chunk => {
                result += chunk;
            });

            res.on('end', () => {
                let stations = JSON.parse(result).network.stations.map(station => {
                    return {
                        name: station.name,
                        latitude: station.latitude,
                        longitude: station.longitude,
                        currentMonkeys: 0,
                        currentUsers: [],
                        stationId: uuidv4()
                    }
                });
                resolve(collection.insertMany(stations))
            })
        }).on('error', error => {
            reject(error);
        }).end();
    })
}

module.exports.stationCollection = {
    name: name,
    options: databaseCfg,
    init: init
};