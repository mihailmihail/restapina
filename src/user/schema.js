'use strict';
/**
 *
 * @type {
 * {
 *  $jsonSchema: {
 *      bsonType: string,
 *      required: [string, string, string, string],
 *      properties: {
 *          pass: {bsonType: string, description: string},
 *          name: {bsonType: string, description: string},
 *          userId: {bsonType: string, description: string},
 *          email: {bsonType: string, description: string},
 *          age: {bsonType: string, description: string, maximum: number, minimum: number}
 *      }}}}
 */
const schema = {
    $jsonSchema: {
        bsonType: "object",
        required: ["userId", "name", "email", "pass"],
        properties: {
            userId: {
                bsonType: "string",
                description: "Must be a string and is required"
            },
            name: {
                bsonType: "string",
                description: "Must be a string and is required"
            },
            email: {
                bsonType: "string",
                description: "Must be a string and is required"
            },
            pass: {
                bsonType: "string",
                description: "User hashed password, must be string"
            },
            age: {
                bsonType: "int",
                description: "Must be integer 0 - 120",
                minimum: 0,
                maximum: 120
            }
        }
    }
};

module.exports = schema;