const { userDb } = require('../db');
const assert = require('assert');

const route = {
    get: get,
    post: post,
    put: put,
    remove: remove
};

/**
 * User get route.
 * @param parameters - URL query parameters
 * @return {Promise}
 */
function get(parameters) {
    assert(parameters.name);
    return userDb.getByName(parameters.name, parameters.limit)
}

/**
 * User post route.
 * @param requestBody - http request body
 * @return {Promise}
 */
function post(requestBody) {
    try {
        let user = JSON.parse(requestBody).user;
        assert(user.email);
        assert(user.name);
        assert(user.pass);
        assert(user.age);
        user.age = parseInt(user.age);

        return userDb.add(user);
    } catch (e) {
        console.log(e);
        throw e;
    }
}

/**
 * User put route.
 * @param requestBody - http request body
 * @return {Promise}
 */
function put(requestBody) {
    try {
        let modify = JSON.parse(requestBody);
        return userDb.modify(modify.age, modify.userId)
    } catch (e) {
        throw e;
    }
}

/**
 * User delete route
 * @param requestBody - http request body
 * @return {Promise}
 */
function remove(requestBody) {
    try {
        let user = JSON.parse(requestBody);
        return userDb.delete(user)
    } catch (e) {
        throw e;
    }
}

module.exports.userRoute = route;
