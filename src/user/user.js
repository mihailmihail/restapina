'use strict';
const userSchema  = require('./schema.js');
const name  = 'users';

const options = {
    validator: userSchema,
    validationLevel: 'moderate'
};

function init(collection) {
    return collection.createIndex({email: 1}, {unique: true});
}

module.exports.userCollection = {
    name: name,
    options: options,
    init: init
};