'use strict';

const assert = require('assert');

const { loginDb } = require('../db.js');

const loginRoute = {
    get: function (authorization) {

        assert(authorization);

        try {
            let auth = authorization.split(' ')[1];
            auth = Buffer.from(auth, 'base64').toString('ascii');

            let [email, pass] = auth.split(':');

            assert(email);
            assert(pass);

            return loginDb.login(email, pass)
        } catch (e) {
            throw e;
        }
    }
};

module.exports.loginRoute = loginRoute;